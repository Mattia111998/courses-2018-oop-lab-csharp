﻿using System;

namespace Properties {

    public class Card
    {
        private readonly string seed;
        private readonly string name;
        private readonly int ordial;

        public Card(string name, string seed, int ordial)
        {
            this.name = name;
            this.ordial = ordial;
            this.seed = seed;
        }

        internal Card(Tuple<string, string, int> tuple)
            : this(tuple.Item1, tuple.Item2, tuple.Item3) { }

        // TODO improve
        public string Seed
        {
            get { return this.seed; }
        }

        // TODO improve
        public string Name
        {
            get { return this.name; }
        }

        // TODO improve
        public int Ordinal
        {
            get { return this.ordial; }
        }

        public override string ToString()
        {
            // TODO understand string interpolation
            return $"{this.GetType().Name}(Name={Name}, Seed={Seed}, Ordinal={Ordinal})";
        }

        public override bool Equals(object obj)
        {
            // TODO improve
            // return base.Equals(obj);
            if (obj is Card)
            {
                Card card = (Card)obj;
                return Name == card.Name 
                    && Seed == card.Seed;
            }
            return false;
        }

        public override int GetHashCode()
        {
            // TODO improve
            return (Name.GetHashCode()
                   * Seed.GetHashCode()
                   * Ordinal.GetHashCode());
        }
    }

}