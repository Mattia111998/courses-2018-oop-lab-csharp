﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        readonly Dictionary<Tuple<TKey1, TKey2>, TValue> map = new Dictionary<Tuple<TKey1, TKey2>, TValue>();

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            get
            {
                return map[Tuple.Create(key1, key2)];
            }
            set
            {
                map[Tuple.Create(key1, key2)] = value;
            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            IList<Tuple<TKey2, TValue>> list = new List<Tuple<TKey2, TValue>>();
            foreach (var item in map)
            {
                if(item.Key.Item1.Equals(key1))
                {
                    list.Add(Tuple.Create(item.Key.Item2, item.Value));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            IList<Tuple<TKey1, TValue>> list = new List<Tuple<TKey1, TValue>>();
            foreach (var item in map)
            {
                if (item.Key.Item2.Equals(key2))
                {
                    list.Add(Tuple.Create(item.Key.Item1, item.Value));
                }
            }
            return list;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            return (IList <Tuple<TKey1, TKey2, TValue>>) map.ToList();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            this[(TKey1)keys1, (TKey2) keys2] ;
        }

        public int NumberOfElements
        {
            get
            {
                return map.Count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
